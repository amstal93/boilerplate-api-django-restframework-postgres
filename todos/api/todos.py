from rest_framework import permissions, viewsets
from rest_framework.response import Response

# from lib.permissions.CustomDjangoModelPermissions import CustomDjangoModelPermissions
from todos.models import Todo
from todos.serializers import TodoSerializer


class TodoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows todos to be viewed or edited.
    """

    queryset = Todo.objects.all().order_by("-updated_at")
    serializer_class = TodoSerializer
    permission_classes = [
        permissions.IsAuthenticated,
        # CustomDjangoModelPermissions,
    ]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        user = self.request.user
        return self.queryset.filter(owner=user)
