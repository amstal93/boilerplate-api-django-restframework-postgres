export const appMeta = {
  title: 'React Boilerplate',
  description: 'An amazing react starter',
  landing: {
    title: 'Get started with React',
  },
  login: {
    title: 'Login',
  },
};
