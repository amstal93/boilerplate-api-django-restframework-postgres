import { initializeStore } from './store';

const store = initializeStore();

export default store;

export type RootState = ReturnType<typeof store.getState>;
