export const SET_CSRF_TOKEN = 'SET_CSRF_TOKEN';

interface SetCsrf {
  type: typeof SET_CSRF_TOKEN;
  payload: {
    csrf: string | null;
  };
}

export type UserActionTypes = SetCsrf;
