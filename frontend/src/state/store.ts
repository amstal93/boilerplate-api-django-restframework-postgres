import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';

import { combineReducers } from 'redux';
import { userReducer } from './user/reducers';
// import { uiReducer } from './ui/reducers';

const rootReducer = combineReducers({
  session: userReducer,
  // store_ui: uiReducer,
});

export function initializeStore() {
  return createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(thunkMiddleware)),
  );
}

// export type RootState = ReturnType<typeof rootReducer>;
