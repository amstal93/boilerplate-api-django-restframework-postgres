import React from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';

import { useAuth } from 'src/contexts/auth-context';
import PublicLayout from '../layouts/public-layout';

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
export default function PublicRoute({ children, ...rest }: RouteProps) {
  const { user } = useAuth();
  return (
    <Route
      {...rest}
      render={({ location }) =>
        !user ? (
          <PublicLayout>{children}</PublicLayout>
        ) : (
          <Redirect
            to={{
              pathname: '/dashboard',
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}
