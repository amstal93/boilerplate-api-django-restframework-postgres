import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { useLocation } from 'react-router';

export default function NotFoundPage() {
  const location = useLocation();
  return (
    <Container maxWidth="sm">
      <Box my={4}>
        <Typography variant="h4" component="h1" gutterBottom>
          No match for <code>{location.pathname}</code>
        </Typography>
      </Box>
    </Container>
  );
}
