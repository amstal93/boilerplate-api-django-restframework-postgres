import React, { useContext, useMemo } from 'react';
import { createContext } from 'react';

import { AxiosError } from 'axios';
import { useHistory } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';

import { sleep } from 'src/utils/sleep';
import { IReactChildren } from 'src/types/react-children';
import store from 'src/state';
import { setCSRF } from 'src/state/user/actions';
import {
  signup as register,
  login as signin,
  logout as logoutUser,
  session,
  getCsrf,
} from 'src/api/auth';
import { appMeta } from 'src/config';
interface IAuthContextProps {
  loading: boolean;
  user: boolean;
  loginErrors: {
    email: null | string;
    password: null | string;
    message: null | string;
  };
  isCheckingSession: boolean;
  login: (
    email: string,
    password: string,
  ) => { message: string } | Promise<void>;
  signup: (
    email: string,
    password: string,
  ) => { message: string } | Promise<void>;
  signupStep: number;
  setSignupStep: React.Dispatch<React.SetStateAction<number>>;
  logout: () => void;
}

const AuthContext = createContext({} as IAuthContextProps);

export function AuthProvider({ children }: IReactChildren) {
  /// //////////////////////////////////////////////////////////////////////////
  /// State
  ///
  ///
  /// ///////////////////////////////////////////////////////////////////////////
  const [loading, setLoading] = React.useState(false);
  const [isCheckingSession, setIsCheckingSession] = React.useState(true);
  const [signupStep, setSignupStep] = React.useState(0);
  const [user, setUser] = React.useState(false);
  const [loginErrors, setLoginErrors] = React.useState<{
    email: null | string;
    password: null | string;
    message: null | string;
  }>({
    email: null,
    password: null,
    message: null,
  });

  /// //////////////////////////////////////////////////////////////////////////
  /// config
  ///
  ///
  /// ///////////////////////////////////////////////////////////////////////////
  const { title } = appMeta;

  /// //////////////////////////////////////////////////////////////////////////
  /// Hooks
  ///
  ///
  /// ///////////////////////////////////////////////////////////////////////////
  const history = useHistory();

  // get csrf token
  React.useEffect(() => {
    const fetchCsrfToken = async () => {
      const r = await getCsrf();
      const csrfToken = r.headers['x-csrftoken'];

      store.dispatch(setCSRF(csrfToken));
    };

    fetchCsrfToken();
  }, []);

  // check session
  React.useEffect(() => {
    const checkSession = async () => {
      await sleep(3000);
      try {
        const r = await session();
        setUser(r.data.isAuthenticated);
      } catch (error) {
        setUser(false);
      } finally {
        setIsCheckingSession(false);
      }
    };

    checkSession();
  }, []);

  /// //////////////////////////////////////////////////////////////////////////
  /// methods
  ///
  ///
  /// ///////////////////////////////////////////////////////////////////////////
  const login = async (email: string, password: string) => {
    setLoading(true);
    await sleep(3000);
    try {
      const r = await signin(email, password);
      setUser(true);
      history.replace('/dashboard');
      setLoading(false);
    } catch (e) {
      const error = e as AxiosError<{ email: string[] }>;

      if (error.response) {
        setLoginErrors({
          ...loginErrors,
          message: error.response.data.email[0],
        });
      }
      console.log(error);
      setLoading(false);
    }
  };

  const signup = async (email: string, password: string) => {
    setLoading(true);
    await sleep(3000);
    try {
      await register(email, password);
      setSignupStep(1);
      setLoading(false);
    } catch (e) {
      const error = e as AxiosError;

      if (error.response) {
        setLoginErrors({ ...loginErrors, message: error.response.data.detail });
      }
      console.log(error);
      setSignupStep(0);
      setLoading(false);
    }
  };

  const logout = async () => {
    try {
      await logoutUser();
      window.location.replace('/login');
    } catch (error) {
      console.log(error);
      document.cookie =
        'sessionid=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
      history.replace('/login');
    }
  };

  /// //////////////////////////////////////////////////////////////////////////
  /// Memo
  ///
  ///
  /// ///////////////////////////////////////////////////////////////////////////
  // Make the provider update only when it should.
  // We only want to force re-renders if the user,
  // loading or error states change.
  //
  // Whenever the `value` passed into a provider changes,
  // the whole tree under the provider re-renders, and
  // that can be very costly! Even in this case, where
  // you only get re-renders when logging in and out
  // we want to keep things very performant.
  const memoedValue = useMemo(
    () => ({
      user,
      loading,
      signupStep,
      loginErrors,
      isCheckingSession,
      signup,
      login,
      logout,
      setSignupStep,
    }),
    [user, loading, signupStep],
  );

  /// //////////////////////////////////////////////////////////////////////////
  /// Content
  ///
  ///
  /// ///////////////////////////////////////////////////////////////////////////
  function renderContent() {
    if (isCheckingSession) {
      return (
        <div style={{ height: '100vh' }}>
          <Helmet>
            <title>Loading | {title}</title>
          </Helmet>
          <Container
            maxWidth="sm"
            style={{
              display: 'flex',
              flexDirection: 'column',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <CircularProgress />
          </Container>
        </div>
      );
    }

    return children;
  }

  return (
    <AuthContext.Provider value={memoedValue}>
      <div
        style={{ height: '100vh', display: 'flex', flexDirection: 'column' }}
      >
        {renderContent()}
      </div>
    </AuthContext.Provider>
  );
}

export function useAuth() {
  return useContext(AuthContext);
}

export { AuthContext };
