from django.contrib import admin
from django.urls import include, path

from rest_framework import routers
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)

from user.api import users as user_api
from todos.api import todos as todo_api


router = routers.DefaultRouter()
router.register(r"users", user_api.UserViewSet)
# router.register(r"groups", core_views.GroupViewSet)
router.register(r"todos", todo_api.TodoViewSet)

urlpatterns = [
    # web paths
    path("", include("core.urls")),
    # other
    path("admin/", admin.site.urls),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    # api routes
    path("api/", include(router.urls)),
    path("api/v1/user/", include("user.api.urls")),
    # YOUR PATTERNS
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    # Optional UI:
    path(
        "api/schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path(
        "api/schema/redoc/",
        SpectacularRedocView.as_view(url_name="schema"),
        name="redoc",
    ),
]
