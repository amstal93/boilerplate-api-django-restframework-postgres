# BOILERPLATE: Django API starter w/ Postgres and Docker

## Stack

- Django + Django Rest Framework
  - https://www.djangoproject.com/
  - https://www.django-rest-framework.org/
- Postgres SQL Database
  - https://www.postgresql.org/
- NGINX reverse proxy
  - https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/
- Docker and Docker-Compose
  - https://www.docker.com/
  - https://docs.docker.com/compose/

## Features

- Authentication
- Groups and permissions
- Automated testing
- API Documentation with Swagger
- CI/CD
- Debugger w/ VSCODE editor

## Getting started

Clone the repo

```sh
git clone https://gitlab.com/beamsies/boilerplate-api-django-restframework-postgres.git my-new-project
```

Change to the directory:

```sh
cd my-new-project
```

Create virual environment:

```sh
python3 -m venv venv
```

Activate python virtual environment:

```sh
source ./venv/bin/activate
```

Install dependencies:

```sh
pip install -r requirements.txt
```

Run local debug:

```sh
docker-compose up --build
```

## Debug the app in local dev

Launch the app

```sh
docker-compose up --build
```

In vscode, go to `Run and Debug` tab

Select the `Python: Remote Attach` option

Click the green play button

Set a breakpoint

## Debug a Test

### 1. Comment out the default debugger step with debugpy.

In the `manage.py` comment out the following lines of code.

```py
if settings.DEBUG:
        import debugpy

        print(os.environ)
        if not os.getenv("RUN_MAIN"):
            debugpy.listen(("0.0.0.0", 5678))
            debugpy.wait_for_client()
            print("debug listening")
```

### 2. Add add debugpy to the test file you want to debug.

In the test file, type `pydebugblock` for the vs-code snippet found in `.vscode/python-snippets.code-snippets.json` file. This snippet will type the following

```py
import debugpy
debugpy.listen(("0.0.0.0", 5678))
debugpy.wait_for_client()
```

This will enable the debugger in the test file you want to debug.

Now you can set breakpoints in vscode and they will be hit during the tests.

### 3. Launch the app with Docker.

Take the app up with the command:

```sh
docker-compose up --build
```

### 4. Run the Django test command against the contaner.

Run the following command:

```sh
docker-compose exec api manage.py test
```

Your debug breakpoint will be hit
