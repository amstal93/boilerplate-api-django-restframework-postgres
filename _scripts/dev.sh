#!/bin/bash

python ./manage.py makemigrations
python ./manage.py migrate --noinput
python ./manage.py create_superuser
python ./manage.py create_public_user_group
python ./manage.py runserver [::]:8000