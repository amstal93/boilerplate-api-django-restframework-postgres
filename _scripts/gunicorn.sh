#!/bin/bash

python ./manage.py migrate --noinput
python ./manage.py create_superuser
python ./manage.py collectstatic --no-input --clear
gunicorn app.wsgi:application --bind 0.0.0.0:8000
