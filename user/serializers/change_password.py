from rest_framework import serializers
from rest_framework import status

from user.models import User
from lib.exceptions.serializers import CustomAPIException


class ChangePasswordSerializer(serializers.ModelSerializer):
    """
    Serializer for registration
    """

    email = serializers.EmailField()
    password = serializers.CharField(
        max_length=200,
        min_length=8,
        style={"input_type": "password"},
        write_only=True,
    )
    confirm_password = serializers.CharField(
        max_length=200, min_length=8, style={"input_type": "password"}, write_only=True
    )

    class Meta:
        model = User
        fields = ["email", "password", "confirm_password"]

    def validate(self, data):
        # check valid pw = pw
        if data["password"] != data["confirm_password"]:
            raise serializers.ValidationError({"password": "Passwords must match."})
        return data

    def save(self):
        """
        Saves a new password for user

        Steps:
        1. validate inputs
        2. check user by email
        3. check token
        3. put password
        """

        # fields
        email = self.validated_data["email"]
        password = self.validated_data["password"]

        user = User.objects.filter(username=email).first()

        # a user with that email address already exists
        if user is None:
            raise CustomAPIException(
                "The information provided does not match our records",
                status_code=status.HTTP_403_FORBIDDEN,
            )

        # save new pw
        user.set_password(password)
        user.locked_out = False
        user.recover_account_token = None
        user.recover_account_token_expires = None
        user.save()

        return user
