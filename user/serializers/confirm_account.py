from rest_framework import serializers


class ConfirmAccountSerializer(serializers.Serializer):
    """
    Confirm the newly created user account
    """

    # email = serializers.EmailField()
    # code = serializers.IntegerField(
    #     max_value=999999,
    #     min_value=000000,
    # )
    token = serializers.CharField()

    class Meta:
        # fields = ["email", "code"]
        fields = ["token"]
