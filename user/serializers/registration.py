import secrets
from datetime import timedelta

from django.core.mail import send_mail
from django.utils import timezone

from rest_framework import serializers
from rest_framework import status

from user.models import User
from lib.exceptions.serializers import CustomAPIException


class RegistrationSerializer(serializers.ModelSerializer):
    """
    Serializer for registration
    """

    email = serializers.EmailField()
    password = serializers.CharField(
        max_length=200,
        min_length=8,
        style={"input_type": "password"},
        write_only=True,
    )
    # confirm_password = serializers.CharField(
    #     max_length=200, min_length=8, style={"input_type": "password"}, write_only=True
    # )

    class Meta:
        model = User
        # fields = ["email", "password", "confirm_password"]
        fields = ["email", "password"]

    def validate(self, data):
        # check valid pw = pw
        # if data["password"] != data["confirm_password"]:
        #     raise serializers.ValidationError({"password": "Passwords must match."})
        return data

    def save(self):
        """
        Saves a new user into the db

        Steps:
        1. validate inputs
        2. check if user exists by email
        3. create and save the user
        """

        # fields
        email = self.validated_data["email"]
        password = self.validated_data["password"]
        # confirm_password = self.validated_data["confirm_password"]

        existing_user = User.objects.filter(username=email).first()

        # a user with that email address already exists
        if existing_user is not None:
            raise CustomAPIException(
                "Email address is already in use. Try to log in instead.",
                "email",
                status_code=status.HTTP_409_CONFLICT,
            )

        # new user
        new_user = User(
            email=email,
            username=email,
        )

        url_token = secrets.token_urlsafe(64)

        expiration = timezone.now() + timedelta(days=1)

        new_link = (
            f"http://localhost:8000/api/v1/user/register/confirm?token={url_token}"
        )

        # save new user
        new_user.set_password(password)
        new_user.registration_token = url_token
        new_user.registration_token_expires = expiration
        new_user.is_staff = False
        new_user.is_active = True
        new_user.is_superuser = False
        new_user.save()

        send_mail(
            "Django App: Confirm your email",
            f"Click the link to verify your email: {new_link}",
            "from_django_app@example.com",
            [new_user.email],
            fail_silently=False,
        )

        return new_user
