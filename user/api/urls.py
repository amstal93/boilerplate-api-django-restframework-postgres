from django.urls import path

from .register import PostRegisterNewUser
from .confirm import ConfirmAccountViewSet
from .login import logout_view, get_csrf, PostLoginRequest
from .session import SessionView

from .reset_password import PostResetPassword, GetResetPasswordToken, PutNewPassword

urlpatterns = [
    path("register", PostRegisterNewUser.as_view()),
    path("login", PostLoginRequest.as_view()),
    path("csrf", get_csrf, name="api-csrf"),
    path("logout", logout_view, name="api-logout"),
    path("session", SessionView.as_view(), name="api-session"),
    path("register/confirm", ConfirmAccountViewSet.as_view()),
    path("account/forgot-pw", PostResetPassword.as_view()),
    path("account/confirm-forgot-pw", GetResetPasswordToken.as_view()),
    path("account/update-pw", PutNewPassword.as_view()),
]
