from datetime import timedelta
import secrets

from django.utils import timezone
from django.core.mail import send_mail

from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics

from user.models import User

from ..serializers.reset_password import (
    ResetPasswordSerializer,
    ConfirmResetPasswordTokenSerializer,
)
from ..serializers.change_password import ChangePasswordSerializer


class PostResetPassword(generics.CreateAPIView):
    serializer_class = ResetPasswordSerializer

    def post(self, request):
        serializer = ResetPasswordSerializer(data=request.data)

        if serializer.is_valid():
            email = serializer.data["email"]

            user = User.objects.filter(username=email).first()

            if user is None:
                return Response(
                    None,
                    status=status.HTTP_403_FORBIDDEN,
                )

            reset_token = secrets.token_urlsafe(64)
            reset_token_expiration = timezone.now() + timedelta(days=1)
            reset_link = (
                f"http://localhost:8000/api/v1/user/account/recover?token={reset_token}"
            )

            user.locked_out = True
            user.recover_account_token = reset_token
            user.recover_account_token_expires = reset_token_expiration
            user.save()

            send_mail(
                "Django App: Confirm your email",
                f"Click the link to reset your password: {reset_link}",
                "from_django_app@example.com",
                [user.email],
                fail_silently=False,
            )

            return Response(status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetResetPasswordToken(generics.ListAPIView):

    serializer_class = ConfirmResetPasswordTokenSerializer
    pagination_class = None

    def get(self, request):
        token = self.request.query_params.get("token")

        user = User.objects.filter(recover_account_token=token).first()

        # check if user found with that reset token
        if user is None:
            return Response(
                {"message": "link expired"}, status=status.HTTP_400_BAD_REQUEST
            )

        # check if token expired
        if user.recover_account_token_expires < timezone.now():
            return Response(
                {"message": "link expired"}, status=status.HTTP_400_BAD_REQUEST
            )

        # return success message
        return Response(status=status.HTTP_200_OK)


class PutNewPassword(generics.UpdateAPIView):
    serializer_class = ChangePasswordSerializer

    def put(self, request):
        serializer = ChangePasswordSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request):
        serializer = ChangePasswordSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
