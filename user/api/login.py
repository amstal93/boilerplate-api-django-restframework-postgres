import json

from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from django.middleware.csrf import get_token
from django.views.decorators.http import require_POST

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics, status

from user.models import User

from ..serializers.login import LoginSerializer

from ..lib.lockout import eval_lockout


def get_csrf(request):
    response = JsonResponse({"detail": "CSRF cookie set"})
    response["X-CSRFToken"] = get_token(request)
    return response


def csrf_failure(request, reason=""):
    return JsonResponse(
        {"detail": "CSRF verification failed. Request aborted."}, status=403
    )


@require_POST
def login_view(request):
    data = json.loads(request.body)
    username = data.get("email")
    password = data.get("password")

    if username is None or password is None:
        return JsonResponse(
            {"detail": "Please provide email and password."}, status=400
        )

    user = User.objects.filter(username=username).first()

    if user is None:
        return JsonResponse(
            {"detail": "That information does not match our records"}, status=404
        )

    user = authenticate(username=username, password=password)

    if user is None:
        return JsonResponse({"detail": "Invalid credentials"}, status=401)

    login(request, user)
    return JsonResponse({"detail": "Successfully logged in."})


def logout_view(request):
    if not request.user.is_authenticated:
        return JsonResponse({"detail": "You're not logged in."}, status=400)

    logout(request)
    return JsonResponse({"detail": "Successfully logged out."})


class PostLoginRequest(generics.CreateAPIView):
    """
    User login
    """

    serializer_class = LoginSerializer

    def post(self, request, format=None):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            user = authenticate(
                request,
                username=serializer.data["email"],
                password=serializer.data["password"],
            )

            # if wrong pw but there is a user we get their profile to do lockout
            profile = User.objects.filter(email=serializer.data["email"]).first()

            if user is None and profile is not None:
                # authentication failed bc user is None
                # and profile is not None bc there is a user
                # this means incorrect pw so we track attempt
                return eval_lockout(profile, serializer)

            if user is not None and not user.email_verified:
                return Response("Email not verified.", status=status.HTTP_403_FORBIDDEN)
            if user is not None:
                login(request, user)
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(
                    "The information you provided does not match our records.",
                    status=status.HTTP_403_FORBIDDEN,
                )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginViewSet(APIView):
    """
    User login
    """

    def post(self, request, format=None):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            user = authenticate(
                request,
                username=serializer.data["email"],
                password=serializer.data["password"],
            )

            # if wrong pw but there is a user we get their profile to do lockout
            profile = User.objects.filter(email=serializer.data["email"]).first()

            if user is None and profile is not None:
                # authentication failed bc user is None
                # and profile is not None bc there is a user
                # this means incorrect pw so we track attempt
                return eval_lockout(profile, serializer)

            if user is not None and not user.email_verified:
                return Response("Email not verified.", status=status.HTTP_403_FORBIDDEN)
            if user is not None:
                login(request, user)
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(
                    "The information you provided does not match our records.",
                    status=status.HTTP_403_FORBIDDEN,
                )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
