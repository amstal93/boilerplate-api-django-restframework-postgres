from django.utils import timezone

# from django.contrib.auth.models import Group

from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics

from drf_spectacular.utils import extend_schema, OpenApiParameter

from user.models import User

from ..serializers.confirm_account import ConfirmAccountSerializer


class ConfirmAccountViewSet(generics.RetrieveAPIView):
    """
    Confirm new user account.
    """

    serializer_class = ConfirmAccountSerializer

    @extend_schema(
        parameters=[OpenApiParameter(name="token", location=OpenApiParameter.QUERY)]
    )
    def get(self, request):

        token = self.request.query_params.get("token")

        user = User.objects.filter(registration_token=token).first()

        # check if user found with that confirm token
        if user is None:
            return Response(
                {"message": "link expired"}, status=status.HTTP_400_BAD_REQUEST
            )

        # check if token expired
        if user.registration_token_expires < timezone.now():
            return Response(
                {"message": "link expired"}, status=status.HTTP_400_BAD_REQUEST
            )

        # get public user group
        # public_user_group = Group.objects.get(name="PublicUserGroup")
        # user.groups.add(public_user_group)

        # if user and token not expired, set email verified
        user.email_verified = True
        user.registration_token = None
        user.registration_token_expires = None
        user.save()

        # return success message
        return Response({"message": "email confirmed"}, status=status.HTTP_200_OK)
