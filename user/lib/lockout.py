from django.utils import timezone
from datetime import timedelta

from rest_framework.response import Response
from rest_framework import status

from user.models import User


def eval_lockout(profile, serializer):
    """
    Evaluate whether to lockout user or remove lockout
    """
    # if locked out but lockout has now expired
    # we reset the attempts and start over
    if profile.locked_out and profile.locked_out_expires < timezone.now():
        profile.locked_out = False
        profile.locked_out_expires = None
        profile.login_attempts = 0
        profile.save()

        return Response(
            "The information you provided does not match our records.",
            status=status.HTTP_403_FORBIDDEN,
        )

    # if locked out already and still before lockout expiry
    if profile.locked_out and profile.locked_out_expires > timezone.now():
        # set new lockout time
        profile.locked_out_expires = timezone.now() + timedelta(minutes=1)
        profile.save()

        return Response(
            "Locked out, try again in 1 minute.",
            status=status.HTTP_403_FORBIDDEN,
        )

    # if not locked out, eval lockout
    if not profile.locked_out:
        # set this attempt first
        profile.login_attempts = profile.login_attempts + 1
        profile.save()

        # get after save
        profile = User.objects.filter(email=serializer.data["email"]).first()

        if profile.login_attempts > 2:
            # set to locked out state
            profile.locked_out = True

            # set lockout expiry
            profile.locked_out_expires = timezone.now() + timedelta(minutes=1)
            profile.save()
            return Response(
                "Locked out, try again in 1 minute.",
                status=status.HTTP_403_FORBIDDEN,
            )
        return Response(
            "The information you provided does not match our records.",
            status=status.HTTP_403_FORBIDDEN,
        )
    return Response(
        "The information you provided does not match our records.",
        status=status.HTTP_403_FORBIDDEN,
    )
